import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { Routes, RouterModule } from '@angular/router';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosViajesEffects, DestinosViajesState, initializeDestinosViajesState, reducersDestinosViajes } from './models/destinos-viajes-state.model';
import { ActionReducerMap, Store } from '@ngrx/store';
import { StoreModule as NgRxStoreModule} from "@ngrx/store";
import { EffectsModule  } from "@ngrx/effects";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

import {HttpClientModule, HttpHeaders, HttpRequest} from '@angular/common/http';

import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';



// app config

export interface AppConfig {

  apiEndpoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {

  apiEndpoint: 'http://localhost:3000'
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
//fin app config





export const childrenRoutesVuelos: Routes = [

{path: '', redirectTo: 'main', pathMatch: 'full'},
{path: 'main', component: VuelosMainComponentComponent},
{path: 'mas-info', component: VuelosMasInfoComponentComponent},
{path: 'id', component: VuelosDetalleComponentComponent}

];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaDestinosComponent},
  { path: 'destino/:id', component: DestinoDetalleComponent},
  { path: 'login', component: LoginComponent},
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard]
  },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [UsuarioLogueadoGuard],
    children: childrenRoutesVuelos

  }


];

//Redux init
export interface AppState{
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = { 
  destinos: reducersDestinosViajes
};

let reducersInitialState = {
  destinos: initializeDestinosViajesState()
};
//fin redux init


//app init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {

  return () => appLoadService.initializeDestinosViajesState();

}

@Injectable()
class AppLoadService {

  constructor(private strore: Store<AppState>, private http: HttpClient ){ }
  async initializeDestinosViajesState(): Promise<any> {
     const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
     const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers: headers});
     const response: any = await this.http.request(req).toPromise();
     this.strore.dispatch(new InitMyDataAction(response.body));

  }
}




@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule
  ],
  providers: [
     AuthService, UsuarioLogueadoGuard,
     {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
     AppLoadService,{ provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
