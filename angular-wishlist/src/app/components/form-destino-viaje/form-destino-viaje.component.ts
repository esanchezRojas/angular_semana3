import { Component, EventEmitter, forwardRef, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { inject } from '@angular/core/testing';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(fb: FormBuilder, @inject(forwardRef(()=> APP_CONFIG)) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([Validators.required, this.nombreValidator, 
      this.nombreValidatorParametrizable(this.minLongitud)])],
      url: ['']
    });

    this.minLongitud = 3;

    this.fg.valueChanges.subscribe((orms: any) => {
        console.log('cambio el formulario: ' );
    });
  }

  ngOnInit() {
    let elementNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elementNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2), 
        debounceTime(120),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
      ).subscribe(AjaxResponse => this.searchResults = AjaxResponse.response);
      


  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }
  
  nombreValidator(control: FormControl): {[ s: string]: boolean}{
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 3){
      return { invalidNombre: true}
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn{
    return (control: FormControl): {[ s:string]: boolean }| null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong){
        return { minLongNombre: true}
      }
  
      return null; 

  }
  }

}
